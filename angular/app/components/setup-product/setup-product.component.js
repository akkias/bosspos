class SetupProductController {
	constructor(API, ToastService, $state) {
		'ngInject';
		this.API = API;
		this.ToastService = ToastService;
		this.state = $state;
	}

    $onInit(){
		this.clearShopData();
        this.businessTypes = ["Food and Drink", "Home and Lifestyle", "Fashion Boutique", "Small Retail"];		
    }
	
	getSelectedBusinessType(){
		if (this.selectedBusinesstype !== undefined) {
			//this.isDefaultValueSelected = false;
			return this.selectedBusinesstype;
		} 
		else {
			return "Please select business type";
		}
	}
	createshop(setupShopForm) {				
		let shop = {
			type: this.selectedBusinesstype,
			name: this.name,
			city: this.city,
			userPIN: this.userPIN,
			roundOf: this.roundOff
		};
		if(setupShopForm.$valid){			
			this.showErrors(setupShopForm.$invalid);			
			this.saveShop(shop);										
		}
		else {
			this.showErrors(setupShopForm.$invalid);
			this.ToastService.show(`Details Entered are not Valid.`);
		}		
	}
	showErrors(flag){
		var allErrors = document.querySelectorAll(".md-validation-error");	
		if(flag){
			allErrors.forEach(function(element) {
				element.style.class = "md-block md-input-invalid";
			}, this);			
		}
		else {
			allErrors.forEach(function(element) {
				element.style.class = "md-block ";
			}, this);
		}		
	}
	saveShop(shop) {
        this.API.all('setupShop').post(shop).then(() => {
            this.ToastService.show(`Shop Created successfully.`);
			this.clearShopData();
			//showErrors(true);
            this.state.go('setup.product');
        });
    }
	clearShopData(){
		this.name = '';
        this.city = '';
		this.userPIN = '';
		this.roundOf = false;		
		this.selectedBusinesstype = undefined;
	}
}

export const SetupProductComponent = {
	templateUrl: './views/app/components/setup-product/setup-product.component.html',
	controller: SetupProductController,
	controllerAs: 'shopDetails',
	bindings: {}
}

class AppSidebarController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const AppSidebarComponent = {
    templateUrl: './views/app/components/app-sidebar/app-sidebar.component.html',
    controller: AppSidebarController,
    controllerAs: 'vm',
    bindings: {}
}

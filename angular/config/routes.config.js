export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

    /*
        data: {auth: true} would require JWT auth
        However you can't apply it to the abstract state
        or landing state because you'll enter a redirect loop
    */

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},
			views: {
				sidebar: {
					templateUrl: getView('sidebar')
				},
				main: {}
			}
		})
        .state('app.landing', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('landing')
                }
            }
        })
        .state('app.dashboard', {
            url: '/app',
            views: {
                'main@': {
                    templateUrl: getView('dashboard')
                }
            }
        })
        .state('app.login', {
			url: '/login',
			views: {
				'main@': {
					templateUrl: getView('login')
				}
			}
		})
        .state('app.register', {
            url: '/register',
            views: {
                'main@': {
                    templateUrl: getView('register')
                }
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@': {
                    templateUrl: getView('forgot-password')
                }
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            views: {
                'main@': {
                    templateUrl: getView('reset-password')
                }
            }
        })
        .state('setup', {
			abstract: true,
            data: {},
			views: {
				sidebar: {
					templateUrl: getView('setup')
				},
				main: {}
			}
		})
        .state('setup.show', {
            url: '/setup',
            views: {
                'main@': {
                    templateUrl: getView('setup')
                }
            }
            
        })
    .state('setup.product', {
            url: '/setup',
            views: {
                'main@': {
                    templateUrl: getView('productSetup')
                }
            }
            
        });
}
